# OpenML dataset: 5-years-historical-stock-quotes

https://www.openml.org/d/43497

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Daily price information for stocks, aggregated into one big file.  

Content
Data was pulled using an api and contains general price information for all stocks that are tradable.  Fields include volume of trades, open and close, as well as high and low prices for the day.  The data goes back from February 2016 and collects daily prices up to February 2021.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43497) of an [OpenML dataset](https://www.openml.org/d/43497). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43497/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43497/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43497/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

